//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>

using namespace std;

char nextcar;				// next car
char current;				// Current car	

void ReadChar(void){		// Read character and skip spaces until 
				// non space character is read
	while(cin.get(current) && (current==' '||current=='\t'||current=='\n'))
	   	cin.get(current);
	nextcar = cin.peek();
}

void Error(string s){
	cerr<< s << endl;
	exit(-1);
}
// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// SimpleExpression := Term {AdditiveOperator Term}
// AdditiveOperator := "+" | "-" | "||"	
void AdditiveOperator(){
	if(current=='+'||current=='-')
		ReadChar();
	else
		if(current=='|')
		{
			ReadChar();
			if(current!='|')
				Error("l'opérateur de comparaison s'écrit '||'");
			else
				ReadChar();
		}
	else
		Error("Opérateur additif attendu");	   // Additive operator expected
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
void MultiplicativeOperator()
{
	if (current == '*' || current == '/' || current == '%')
		ReadChar();
	else
		if (current == '&')
			ReadChar(nextcar);
		if (nextcar != '&')
			return Error("L'opérateur 'et' s'écrit avec deux &")
		else
			ReadChar();
	else
		Error("opérateur de multiplication attendu ! ")
}
		
void ComparativeOperator(void){
	if(current=='<'||current=='>'||current=='=')
		ReadChar();
	else
		Error("Opérateur comparatif attendu");	   // Comparative operator expected
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
void DoubleComparativeOperator(void){
	if((current=='<' && nextcar=='=')||(current=='>' && nextcar =='=')||(current=='<' && nextcar == '>'))
		ReadChar();
	else
		ComparativeOperator();
}

// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Number := Digit{Digit}
void Digit(void){
	if((current<'0')||(current>'9'))
		Error("Chiffre attendu");		   // Digit expected
	else{
		cout << "\tpush $"<<current<<endl;
		ReadChar();
	}
}

// Letter := "a"|...|"z"
void Letter(void)
{
	if ((current < 'a') || (current > 'z')) // on fait comme au dessus, current < a se réfère au code ASCII pas besoin donc de préciser chaque chaque lettres
		Error("Lettre attendue"); //on suit le même schéma que pour digit
	else
	{
		cout << "\t PUSH" << current << endl; // on affiche la lettre actuellement lu
		ReadChar();
	}
}

void ArithmeticExpression(void);			// Called by Term() and calls Term()

void Term(void){
	if(current=='('){
		ReadChar();
		ArithmeticExpression();
		if(current!=')')
			Error("')' était attendu");		// ")" expected
		else
			ReadChar();
	}
	else 
		if (current>='0' && current <='9')
			Digit();
	     	else
			Error("'(' ou chiffre attendu");
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor


void ArithmeticExpression(void){
	char adop;
	Term();
	while(current=='+'||current=='-'){
		adop=current;		// Save operator in local variable
		AdditiveOperator();
		Term();
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		if(adop=='+')
			cout << "\taddq	%rbx, %rax"<<endl;	// add both operands
		else
			cout << "\tsubq	%rbx, %rax"<<endl;	// substract both operands
		cout << "\tpush %rax"<<endl;			// store result
	}

}

void Comparative(void)
{
    char opcomp; //operateur comparaison
   ArithmeticExpression();//recupere la valeur en haut de la pile
    opcomp = current; // recupere l'operateur
    ComparativeOperator(); //operateur comparatif
   ArithmeticExpression();/*analyse la seconde expression. Au sommet de la pile j'aurais 
    le résultat de cette expression puis le résultat de la premiere expression.
    il faut maintenant les comparer. */
    cout << "pop %rbx" << "\t #résultat de l'expression 2" << endl; // \t signifie tabulation
    cout << "pop %rax" << "\t #résultat de l'expression 1" << endl;
    switch (opcomp)
    {
        case '=': cout << "\t SUBq    %rax, %rbx" << endl;
        	ReadChar();
        	ReadChar(nextcar);
        //opérateur différent
        //si rax = rbx alors rbx - rax = 0
        // sinon ça vaudra pas 0. rbx - rax != 0
			cout << "\t push %rbx" << endl;
			break;
    
        case '<' : cout <<"\t cmpq %rax, %rbx"<< endl;
            cout <<"\t push %rbx" << endl;
            break;
	
		case '>' : cout <<"\t cmpq %rax, %rbx"<< endl;
            cout <<"\t jg greater" << endl;
            cout << "\t movq s0, %rax" << endl;
            cout << "\t jmp Suite" << endl;
			break;

		case '<=' : 

		case '>=' :
			cout <<"\t cmpq %rax, %rbx"<< endl;
			cout << "\t jge greaterEqual" << endl;
			cout << "\t movq s0, %rax" << endl;
            cout << "\t jmp Suite" << endl;
		
		case '<>' :
			cout << "\t cmpq %rax, %rbx" << endl;
			cout << "\t jne notEqual" << endl;
            cout << "\t movq s0, %rax" << endl;
            cout << "\t jmp Suite" << endl;

		case '!=' :


		case '==' :

    }
}
/*
void Multiplicative(void)
{
	char mulop;

}
*/

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;

	// Let's proceed to the analysis and code production
	ReadChar();
	ArithmeticExpression();
	ReadChar();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(cin.get(current)){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}